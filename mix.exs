defmodule Luze.Mixfile do
  use Mix.Project

  def project do
    [
      app: :luze,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :hackney, :poison, :cowboy, :plug],
      mod: {Luze.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:hackney, "~> 1.2.0"},
      {:cowboy, "~> 1.1"},
      {:plug, github: "elixir-plug/plug"},
      {:poison, "~> 2.0"},
      {:credo, github: "rrrene/credo", only: [:dev, :test]}
    ]
  end
end
