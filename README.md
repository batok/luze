# Luze

## Web application with nice features
### WIP - not yet available

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `luze` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:luze, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/luze](https://hexdocs.pm/luze).

## This was added from web ide