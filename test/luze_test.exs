defmodule LuzeTest do
  use ExUnit.Case
  doctest Luze

  test "greets the world" do
    assert Luze.hello() == :world
  end

  test "factorial doing right" do
     assert Luze.factorial(4) == 24
  end
end
