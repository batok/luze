defmodule Luze do
  @moduledoc """
  Documentation for Luze.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Luze.hello
      :world

  """
  def hello do
    :world
  end

  @doc """
  Factorial.

  ## Examples

       iex> Luze.factorial 5
       120

  """

  def factorial(0, f) do
    f
  end

  def factorial(i, f) do
    factorial(i - 1, f * i)
  end

  def factorial(0), do: 1
  def factorial(n), do: factorial(n, 1)
end
